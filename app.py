import uvicorn

if __name__ == "__main__":
    try:
        uvicorn.run("main:app", port=8001)
    except Exception as e:
        print("Unexpected due to >> ", e)
