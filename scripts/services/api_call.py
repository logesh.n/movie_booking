# importing libraries

from fastapi import APIRouter

from scripts.constants.endpoints import Routes
from scripts.constants.model import TicketBooking
from scripts.core.handlers.api_functions import Tickets
from scripts.core.handlers.create_redis_db import redis_db_create
from scripts.core.handlers.get_all_seats import seat_availability
from scripts.config.databases.redis.redis_connection import connection_one

# Create FastAPI instance
router = APIRouter()


# Root
@router.get(Routes.root)
async def root():
    return {"Message": "It's Working!"}


@router.get(Routes.seat_availability)
def check_seats(ticket_class: str):
    message = seat_availability(connection_one, ticket_class)
    return message


@router.post(Routes.book_ticket, tags=["Ticket Booking"])
async def book_ticket(details: TicketBooking):
    try:
        return Tickets().insert_data_into_db(details)
    except Exception as e:
        print("error ", e)


@router.get(Routes.get_booking_details, tags=["get booking details"])
def get_booking_details():
    try:
        Tickets().get_details()
        return {"data": "list all the data"}
    except Exception as e:
        print(e)


@router.get(Routes.get_booking_details_between, tags=["get booking details between"])
def get_booking_details_between(from_date: str, to_date: str):
    Tickets().get_details_between(from_date, to_date)
    return {"Message": f"Data stored on file from {from_date} to {to_date}."}


@router.get(Routes.show_begin, tags=["show_begin"])
def generate_seats():
    try:
        redis_db_create(connection_one, "gold", 30, 300)
        redis_db_create(connection_one, "silver", 25, 125)
        return "booking started"
    except Exception as e:
        print(e)
