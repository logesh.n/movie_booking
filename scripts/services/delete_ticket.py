from fastapi import APIRouter
from scripts.constants.endpoints import Routes
from scripts.core.handlers.api_functions import Tickets

delete_ticket = APIRouter()


@delete_ticket.post(Routes.delete_a_ticket, tags=["delete a ticket"])
def delete_tickets(tid: int):
    try:
        res = Tickets().delete_row(tid)
        return res
    except Exception as e:
        print(e)


@delete_ticket.get(Routes.cancel_all_tickets, tags=["cancel show"])
async def cancel_all_tickets():
    Tickets().cancel_tickets()
    return {"Message": "Cancelled all Tickets"}
