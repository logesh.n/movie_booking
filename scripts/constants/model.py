from pydantic import BaseModel


# Model for fetching details from user
class TicketBooking(BaseModel):
    mobile_no: int
    preferred_class: str
    no_of_tickets: int
    seat_numbers: str
    age: int
