class Routes:
    root = "/"
    book_ticket = "/book_ticket/"
    delete_a_ticket = "/delete_ticket{ticket_id}"
    cancel_all_tickets = "/cancel_all_tickets/"
    seat_availability = "/seat_availability/"
    get_booking_details = "/get_booking_details/"
    show_begin = "/show_begin"
    get_booking_details_between = "/get_booking_details_between/"
