# Importing redis library
import redis as r
from scripts.config.configuration import redis_host, redis_port, redis_database_one, redis_database_two


try:
    connection_one = r.Redis(host=redis_host, port=redis_port, db=redis_database_one)
    connection_two = r.Redis(host=redis_host, port=redis_port, db=redis_database_two)

except Exception as e:
    print("Error in Connection >> ", e)
