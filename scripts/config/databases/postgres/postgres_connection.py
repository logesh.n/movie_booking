from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from scripts.config.configuration import postgres_uri
from scripts.config.databases.postgres.model import Base


def db_connect():
    try:
        engine = create_engine(postgres_uri)
        sessions = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        Base.metadata.create_all(engine)
        session = sessions()
        return session
    except Exception as e:
        print("Exception due to >> ", e)
