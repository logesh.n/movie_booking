from datetime import datetime
from sqlalchemy import Column, Integer, String, Date, Boolean, BIGINT
from sqlalchemy.orm import declarative_base

from scripts.config.configuration import table_name

Base = declarative_base()


# create BookingDetails class
class BookingDetails(Base):
    __tablename__ = table_name
    booking_id = Column(Integer, primary_key=True)
    mobile_no = Column(BIGINT)
    preferred_class = Column(String)
    no_of_tickets = Column(Integer)
    seat_numbers = Column(String)
    date_of_purchase = Column(Date, default=datetime.today())
    age = Column(Integer)
    deleted = Column(Boolean, default=False)
