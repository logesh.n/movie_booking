from configparser import ConfigParser
import os
from dotenv import load_dotenv

try:
    load_dotenv()
    postgres_uri = os.getenv("POSTGRES_URI")
    table_name = os.getenv("TABLE_NAME")
    token = os.getenv("TOKEN")

    config = ConfigParser()
    config.read(r'C:\Users\logesh.n\PycharmProjects\movie_booking\conf\application.conf')

    # redis connection
    redis_host = config.get('redis', 'redis_host')
    redis_port = config.get('redis', 'redis_port')
    redis_database_one = config.get('redis', 'redis_database_one')
    redis_database_two = config.get('redis', 'redis_database_two')

    # mqtt connection
    broker = config.get('mqtt', 'broker')
    port_mqtt = config.get('mqtt', 'port_mqtt')

    # print("success")
except Exception as e:
    print("Exception occurred due to >> ", e)
