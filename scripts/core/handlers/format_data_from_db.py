def clean_record(records):
    dict_records = [record.__dict__ for record in records]
    # Remove any keys from the dictionaries that start with an underscore
    dict_records_clean = [
        {
            key: values for key, values in record.items() if not key.startswith('_')
        }
        for record in dict_records
    ]
    return dict_records_clean
