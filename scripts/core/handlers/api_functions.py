import json
from datetime import datetime
import pandas as pd
import paho.mqtt.client as mqtt
from fastapi import HTTPException
from scripts.config.configuration import token
from scripts.core.handlers.seats_availability import check_seat_avail
from scripts.core.handlers.format_data_from_db import clean_record
from scripts.config.databases.redis.redis_connection import connection_one
from scripts.config.databases.postgres.postgres_connection import db_connect
from scripts.config.databases.postgres.model import BookingDetails
from scripts.config.databases.redis.redis_connection import redis_host


class Tickets:
    def __init__(self):
        self.session = db_connect()
        self.client = mqtt.Client()
        self.client.connect(redis_host, 1883, 60)
        self.booked_list = []

    def insert_data_into_db(self, details):
        # add new booking details
        try:
            # Check if the data entered
            # if 5 >= details.no_of_tickets > 0:
            #     raise HTTPException(status_code=400, detail="Can book only 5 tickets at a time.")
            if details.preferred_class not in ["gold", "silver"]:
                raise HTTPException(status_code=400, detail="Invalid class type")
            seats = details.seat_numbers.split(",")
            integer_list = [int(item) for item in seats]
            if len(integer_list) != details.no_of_tickets:
                raise HTTPException(status_code=400, detail="Number of seat numbers must match the number of tickets")

            for seat_ in integer_list:
                if seat_ not in self.booked_list:
                    if not check_seat_avail(conn, details.preferred_class, int(seat_)):
                        self.booked_list.append(seat_)
            if self.booked_list:
                raise HTTPException(status_code=400,
                                    detail=f"Seat number that are already booked are : {self.booked_list} \n"
                                           f"seat number you are going to book  are {integer_list}")

            payload = {details.preferred_class: integer_list}
            self.client.publish(token, json.dumps(payload))
            new_booking = BookingDetails(mobile_no=details.mobile_no,
                                         preferred_class=details.preferred_class,
                                         no_of_tickets=details.no_of_tickets,
                                         seat_numbers=details.seat_numbers,
                                         age=details.age)

            self.session.add(new_booking)
            self.session.commit()
            return {"message": "Ticket booking successful"}
        except Exception as e:
            return e

    def cancel_tickets(self):
        try:
            query = self.session.query(BookingDetails).update({BookingDetails.deleted: True})
            # Commit the changes to the database
            self.session.commit()
            if query is not None:
                # Publish the Message
                topic = "ticket2/cancel_all"
                message = "Show Cancelled"
                self.client.publish(topic, message)
        except Exception as e:
            logger.error(e)

    def delete_row(self, booking_id):
        try:
            # Soft delete a row in the ticket_details table
            ticket = self.session.query(BookingDetails).filter(BookingDetails.booking_id == booking_id,
                                                               BookingDetails.deleted == False).first()
            if ticket is not None:
                ticket.deleted = True
                self.session.commit()
                no_seats = ticket.seat_numbers
                data_of_list = no_seats.split(",")
                integer_list = [int(item) for item in data_of_list]
                data = {ticket.preferred_class: integer_list}
                self.client.publish("ticket2/delete", json.dumps(data))
                return data
            else:
                print(f"Ticket with id {booking_id} is not exist")
                logger.error(f"Ticket with id {booking_id} is not exist")
                return {"message": f"ticket with id {booking_id} not exist", "status": "Ticket not exist",
                        "data": booking_id}

        except Exception as e:
            logger.error(e)

    def get_details(self):
        try:
            records = self.session.query(BookingDetails).all()
            cleaned_data_dict = clean_record(records)
            # Use pandas to convert the list of dictionaries to a DataFrame
            df = pd.DataFrame(cleaned_data_dict)
            print("-------------------------------Booking Details-------------------------------")
            print(df)
            # Use pandas to save the DataFrame to an Excel file
            df.to_excel('temp/booking_details.xlsx', index=False)
        except Exception as e:
            logger.error(e)

    def get_details_between(self, from_date, to_date):
        try:
            records = self.session.query(BookingDetails).filter(
                BookingDetails.date_of_purchase.between(datetime.strptime(from_date, "%Y-%m-%d").date(),
                                                        datetime.strptime(to_date, "%Y-%m-%d").date())).all()
            cleaned_data_dict = clean_record(records)
            # Use pandas to convert the list of dictionaries to a DataFrame
            df = pd.DataFrame(cleaned_data_dict)
            # Use pandas to save the DataFrame to an Excel file
            df.to_excel(f'temp/booking_from_{from_date}_to_{to_date}_.xlsx', index=False)
        except Exception as e:
            logger.error(e)
