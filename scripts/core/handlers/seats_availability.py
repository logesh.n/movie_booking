import pickle


# check for the seat availability
def check_seat_avail(connection, ticket_class, seats_needed):
    try:
        class_lists = {
            "gold": pickle.loads(connection.get("gold")),
            "silver": pickle.loads(connection.get("silver"))
        }
        # get the availability of seat
        if ticket_class not in class_lists:
            return None
        flag = 0
        for seats in class_lists[ticket_class]:
            if seats_needed in seats:
                flag += 1
                return True
        if flag == 0:
            return False
    except Exception as e:
        print(e)
