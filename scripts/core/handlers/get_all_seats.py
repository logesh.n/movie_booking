# check if class seats are available
import pickle


def data_decode(val):
    # decode the val to integers
    if int(val.decode('UTF-8')):
        return True
    else:
        return False


def create_json_view(connection, ticket_class):
    mapper_ticket_class = {
        "gold": pickle.loads(connection.get("gold")),
        "silver": pickle.loads(connection.get("silver"))
    }

    class_list = mapper_ticket_class[ticket_class]
    data = {"message": "Seat Availability"}
    # create the json for the user to view
    for i, row in enumerate(class_list):
        data["Row {}".format(i + 1)] = ", ".join(map(str, row))
    return data


# check if seats are available if then return seats else then return class that's available or return houseful
def seat_availability(connection, ticket_class):
    try:
        # counter for the checking if one class is full then get the other class
        counter = None
        class_avail = {
            "gold": connection.get("gold_available_seats"),
            "silver": connection.get("silver_available_seats")
        }

        # decode the data and the seat availability
        for key, val in class_avail.items():
            check_status = data_decode(val)
            if key == ticket_class and check_status:
                return create_json_view(connection, ticket_class)
            elif key is not ticket_class and check_status:
                counter = key
                continue
        if counter is not None:
            return {"message": "Seat Availability", "Status": "Not Available", counter: "Available"}
        else:
            return {"HouseFull"}
    except Exception as e:
        print(e)
