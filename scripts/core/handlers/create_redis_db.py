# create redis db
import pickle


def redis_db_create(connection, ticket_class, seats, total_count):
    try:
        mapper_ticket_class = {
            "gold": lambda count: connection.set("gold_available_seats", count),
            "silver": lambda count: connection.set("silver_available_seats", count)
        }

        # if the class is not present
        if ticket_class not in mapper_ticket_class:
            return None

        # getting the class from the mapper
        mapper_ticket_class[ticket_class](total_count)

        list_ = []
        # appending to the list for creating the redis db
        for row in range(1, total_count, seats):
            list_.append([seat for seat in range(row, row + seats)])

        return True if connection.set(ticket_class, pickle.dumps(list_)) else False
    except Exception as e:
        print(e)
