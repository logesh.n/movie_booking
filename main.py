import uvicorn
from fastapi import FastAPI
from scripts.services.api_call import router
from scripts.services.delete_ticket import delete_ticket

app = FastAPI()
app.include_router(router)
app.include_router(delete_ticket)
if __name__ == "__main__":
    try:
        uvicorn.run(app, port=8001)
    except Exception as e:
        print("Error-", e)
